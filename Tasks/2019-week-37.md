WEEK 37 (2019), Milestone [12.3](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&assignee_username=matejlatin&milestone_title=12.3) - W4

* [ ] Authors vs Commenters vs Assignees on Productivity Analytics (gitlab-org/gitlab-ee#13267)
* [x] Policies (gitlab-org/gitlab-ee#7625)
  * [x] Catchup with Jeremy, plan next steps
* [ ] ~Stretch Add issues, commits, jobs to productivity analytics (gitlab-org/gitlab-ee#13300)
* [x] Look at [customer feedback video](https://drive.google.com/file/d/1uOD9fqVC4xYhVeQP78O8RHNRUEQ1U8y3/view)
* [ ] Check the Superhuman[ Onboarding](https://growth.design/case-studies/superhuman-user-onboarding/)
* [ ] Check the status of [colour audit](https://gitlab.com/groups/gitlab-org/-/epics/897)
* [x] Update the designs of the segmented control design pattern (gitlab-org/gitlab-services/design.gitlab.com#375)
* [x] Update the Q3 OKRs issue (gitlab-org/gitlab-design#555)—add Pajamas components etc.
* [x] Segmented control comparison table (gitlab-org/gitlab-services/design.gitlab.com#387)

---
Unplanned tasks:

* [x] Check gitlab-org/gitlab-ui#410
* [x] Update [code hotspots](https://gitlab.com/gitlab-org/gitlab-ee/issues/12683#note_213484858) designs
* [x] Check [this MR](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/31619#note_214035285)
* [ ] Show WIP MRs in [productivity analytics](https://gitlab.com/gitlab-org/gitlab-ee/issues/14759#note_212853289)
* [x] Create a UX buddy issue for Nick (gitlab-org/gitlab-design#599)
* [x] Wipe old laptop (gitlab-com/business-ops/itops/issue-tracker#148)
* [x] Check out the [Growth Fast Boot deck](https://docs.google.com/presentation/d/1Eeh-Nm7n1Z14HZIp6REZ-0vId6PcAbGVjlW4EgyYeRs/edit#slide=id.g2823c3f9ca_0_30)
* [x] Add policies to Group feedback call for next week

Ongoing tasks:

* Dogfood design management (gitlab-com/www-gitlab-com#5218)
* UX Buddy to Nick (gitlab-org/gitlab-design#599) ([Onboarding buddies](https://about.gitlab.com/handbook/general-onboarding/onboarding-buddies/)
* Test Hadron App (https://hadron.app/dashboard)


Comments:

* *Lots of unplanned tasks this week, it was hard to be focused.*

TODOs for next week:

* Update form (buttons) guidelines—not disabling buttons by default should mainly be applied to main form submit buttons.
* Policies -> Mural, prepare for presentation