WEEK 24 (2020), Milestone [13.1](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&assignee_username=matejlatin&milestone_title=13.1) - W4

* [ ] Move the buy CI mins flow into .com (https://gitlab.com/gitlab-org/growth/product/-/issues/1439)
  * [x] Get feedback
  * [ ] Prepare an MVC solution
* [x] Read https://docs.google.com/document/d/1Xw6XhQilhmgG_faOHWHIEMl2EqZFMTqAGbS2ho1Tvwk/edit
* [x] Read https://gitlab.com/groups/gitlab-org/growth/-/epics/38
* [x] Watch https://www.youtube.com/watch?v=SSo97VwVn4Y
* [ ] User interview notes
* [ ] ~Stretch Node.js & Express - 2 chapters
* [ ] ~Stretch Read 3 growth blog posts
* [ ] ~Stretch Read [Developer onboarding](https://about.gitlab.com/handbook/developer-onboarding/)
* [ ] ~Stretch Read the [remote playbook](https://about.gitlab.com/resources/downloads/ebook-remote-playbook.pdf)

---
Unplanned tasks:

* [x] Review gitlab-org/gitlab!33194
* [ ] Look into [Growth job map](https://www.figma.com/proto/S177n19OgDIxmivfJJSqIQ/Jobs-to-be-done?node-id=26838%3A531&viewport=8900%2C273%2C1.960299015045166&scaling=min-zoom)

TODOs for next week:

* ...

Ongoing tasks:

* [Navigation improvements](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1197)
* [Growth issue structure](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/97)
* Think about the [Growth UX page content](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/growth/)
* Research [code-based design tools](https://gitlab.com/gitlab-org/gitlab-design/issues/241)
* Map GitLab's business model (gitlab-org/gitlab-design#803)
* Watch [Growth Video Demos](https://www.youtube.com/playlist?list=PL05JrBw4t0Kr_-AowJmbhGk9yj_zIZySf)
* [Master user journey for Growth](https://gitlab.com/gitlab-org/growth/ui-ux/issues/68)
* [Pilot UX issue weights](https://gitlab.com/gitlab-org/gitlab-design/issues/968)