# Matej — README.md

I created this personal README document to help you get to know me and how I work. This document is inspired by Rayana Verissimo's [README](https://gitlab.com/rverissimo/readme/blob/master/README.md) which she based on [Eric Johnson's](https://about.gitlab.com/handbook/engineering/erics-readme/) and [Jason Lenny's](https://gitlab.com/jlenny/focus/) READMEs.

* [GitLab Handle](https://gitlab.com/matejlatin)
* [Team page](https://about.gitlab.com/company/team/#matejlatin)

## About me

My name is Matej Latin (“j” is pronounced as “y”) and I am a Senior Product Designer for [Growth](https://about.gitlab.com/handbook/product/growth/) at GitLab.

* I currently live in Ljubljana, Slovenia 🇸🇮 with my wife Valentina and our cat Blacky.
* I’m from Slovenia but I lived and worked in Germany for 2 years, London, England for 3 years and in Edinburgh, Scotland for 1 year. [Edinburgh](https://www.youtube.com/watch?v=pqEfx0fUwMA) was by far my favourite city.
* I never studied design, HCI or anything related. I graduated in Economics (Management) and my design journey started as a hobby.
* I’m a typography geek and I wrote a book about web typography — [Better Web Typography for a Better Web](https://betterwebtype.com/web-typography-book/).
* I’m an introvert which is something that I struggled with until I read [Quiet](https://www.goodreads.com/book/show/8520610-quiet) by Susan Cain. This mostly means that I listen more than I talk and that I love being alone at times.
* I’m a bookworm. I tend to read around 25 books per year, my personal record is 40. ([My Goodreads profile](https://www.goodreads.com/author/show/17065301.Matej_Latin))
* I run a personal blog at [matejlatin.co.uk](https://matejlatin.co.uk)

## How I work

* My timezone is [CET](https://time.is/CET)
* My working hours: 8am to 5pm  with a 1-hour lunch break at 2pm.
* I prefer fixed working hours because it allows me to be more productive and efficient.
* I block out 3-4 hours each day (in the morning) for meaningful work.


### My typical week
* I have most of my meetings on Tuesdays and Wednesdays and like to keep Thursdays and Fridays meetings-free. 
* Each Friday I prepare a plan for the next week.

### My typical day

* I get up at 6 on weekdays.
* I start working at 8 and take my first break at 11:30-12:00. I’m not very responsive during these hours (focus time).
* I check my email after lunch at 3pm and prefer to have meetings in the afternoon hours.

You can schedule a 30 min call with me via [Calendly](https://calendly.com/matejlatin).

## What’s keeping me busy

I have my own OKRs, check out [my current OKRs](https://www.notion.so/matejlatin/Current-OKRs-104a18ed44db4fa3bb027b57421e3d4c), to see my past OKRs and their scores check out my [archive of OKRs](https://www.notion.so/matejlatin/OKRs-Archive-f61bc32c146e44c89fc4d4cc8a5e1486).

To see what I'm currently working on [check my current tasks](https://www.notion.so/matejlatin/Current-week-87ed1032e9ad4f569095717ab8e56772)

## How to reach me
* **E-mail**: I read my emails once a day. You should not feel obligated to read or respond to any of emails you receive from me outside of your normal hours.
* **Slack**: Direct message or mention is probrably the best way to reach me during the day. I will always have Slack open during business hours but I have notifications disabled so may take some time before I respond. I do not have Slack installed on my personal phone, so I will respond once I am back at the computer.

## My goals as a designer at GitLab
* Help drive the improvement of Product Design at GitLab by giving feedback and mentoring designers and pushing for collaboration among them.
* Take a step back and look at things holistically. Solve the product design problems, not just interaction and UX design problems.
* Be humble and keep learning about GitLab and how it helps our users.
* Write blog posts about our design challenges and help promote the GitLab's Product Design department to a wider audience. 
* Help colleagues be more productive and effective as remote workers, promote remote work and why GitLab is so successfull at it. 

## Trivia
* I love washing my car, I can literally spend 4,5 or 6 hours washing and detailing it. 🚙✨
* I used to play the guitar in a band. 🎸
* I’m slightly colour blind. 🎨👀