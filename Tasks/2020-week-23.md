WEEK 23 (2020), Milestone [13.1](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&assignee_username=matejlatin&milestone_title=13.1) - W3

* [x] Review https://gitlab.com/gitlab-org/gitlab/-/merge_requests/33210#note_351315501
* [x] Review https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/50635#note_351399455
* [x] Invite members - Later iterations (https://gitlab.com/gitlab-org/gitlab/-/issues/217922)
* [x] Prompt a user to invite more team members (https://gitlab.com/gitlab-org/gitlab/-/issues/8063) → Reach a decision
* [x] [Move the "buy add on pipeline minutes" flow into the app](https://gitlab.com/gitlab-org/growth/product/-/issues/1439)
* [ ] User interview notes
* [ ] ~Stretch Node.js & Express - 2 chapters
* [ ] ~Stretch Read 3 growth blog posts
* [ ] ~Stretch Read [Developer onboarding](https://about.gitlab.com/handbook/developer-onboarding/)
* [ ] ~Stretch Read the [remote playbook](https://about.gitlab.com/resources/downloads/ebook-remote-playbook.pdf)

---
Unplanned tasks:

* [ ] Read https://docs.google.com/document/d/1Xw6XhQilhmgG_faOHWHIEMl2EqZFMTqAGbS2ho1Tvwk/edit
* [ ] Read https://gitlab.com/groups/gitlab-org/growth/-/epics/38

TODOs for next week:

* Watch https://www.youtube.com/watch?v=SSo97VwVn4Y

Ongoing tasks:

* [Navigation improvements](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1197)
* [Growth issue structure](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/97)
* Think about the [Growth UX page content](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/growth/)
* Research [code-based design tools](https://gitlab.com/gitlab-org/gitlab-design/issues/241)
* Map GitLab's business model (gitlab-org/gitlab-design#803)
* Watch [Growth Video Demos](https://www.youtube.com/playlist?list=PL05JrBw4t0Kr_-AowJmbhGk9yj_zIZySf)
* [Master user journey for Growth](https://gitlab.com/gitlab-org/growth/ui-ux/issues/68)
* [Pilot UX issue weights](https://gitlab.com/gitlab-org/gitlab-design/issues/968)