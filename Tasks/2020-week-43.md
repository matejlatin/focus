WEEK 43 (2020), Milestone [13.6](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&assignee_username=matejlatin&milestone_title=13.5) - W1

* [x] Think about seat expansion UX strategy
  * [x] Review https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/216
  * [x] Review https://docs.google.com/document/d/128fidRnx1IupTZ2Bc9mjq-YlU4Ir_o8448W-qluGe0E/edit
* [x] Document nudge in the Pajamas
  * [x] Start the MR
  * [x] Outline
* [ ] Look into https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/557
* [x] 13.6 Plan
* [ ] ~Stretch Read https://about.gitlab.com/resources/whitepaper-reduce-cycle-time/
* [ ] ~Stretch Read [Developer onboarding](https://about.gitlab.com/handbook/developer-onboarding/)
* [ ] ~Stretch Read the [remote playbook](https://about.gitlab.com/resources/downloads/ebook-remote-playbook.pdf)

---
Unplanned tasks:

* [x] Review https://gitlab.com/gitlab-org/gitlab/-/merge_requests/42398#note_429112843
* [x] Review https://gitlab.com/gitlab-org/gitlab/-/merge_requests/45689#note_433032716

TODOs for next week:

* [ ] ...

Burnout

**Overall: `3.1 / 6`**
  * Exhaustion: `3 / 6`
  * Cynicism: `3 / 6`
  * Depersonalization: `2 / 6`
  * Self-inefficacy: `4.5 / 6`

Ongoing tasks:

* Think about gitlab-org/gitlab#225331
* [Navigation improvements](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1197)
* Think about the [Growth UX page content](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/growth/)
* Research [code-based design tools](https://gitlab.com/gitlab-org/gitlab-design/issues/241)
* Map GitLab's business model (gitlab-org/gitlab-design#803)
* Watch [Growth Video Demos](https://www.youtube.com/playlist?list=PL05JrBw4t0Kr_-AowJmbhGk9yj_zIZySf)