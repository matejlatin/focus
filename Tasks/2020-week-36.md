WEEK 36 (2020), Milestone [13.4](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&assignee_username=matejlatin&milestone_title=13.4) - W3

* [x] Resume Homepage redesign
  * [x] Wrap up Strategy for the redesign
  * [x] Video walkthrough for the strategy
* [x] Wrap up Blog post ([issue](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8927))
* [x] Resume button migration work
* [ ] ~Stretch Read https://about.gitlab.com/resources/whitepaper-reduce-cycle-time/
* [ ] ~Stretch Read [Developer onboarding](https://about.gitlab.com/handbook/developer-onboarding/)
* [ ] ~Stretch Read the [remote playbook](https://about.gitlab.com/resources/downloads/ebook-remote-playbook.pdf)

---
Unplanned tasks:

* [ ] ...

TODOs for next week:

* [ ] ...

Ongoing tasks:

* Think about gitlab-org/gitlab#225331
* [Navigation improvements](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1197)
* Think about the [Growth UX page content](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/growth/)
* Research [code-based design tools](https://gitlab.com/gitlab-org/gitlab-design/issues/241)
* Map GitLab's business model (gitlab-org/gitlab-design#803)
* Watch [Growth Video Demos](https://www.youtube.com/playlist?list=PL05JrBw4t0Kr_-AowJmbhGk9yj_zIZySf)