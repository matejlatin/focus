WEEK 2 (2020), Milestone [12.7](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&assignee_username=matejlatin&milestone_title=12.7) - W4

* [x] Catch up on things
* [x] Individual growth plan (gitlab-org/gitlab-design#820)
* [ ] Create designs for the [MR without pipeline experiment](https://gitlab.com/groups/gitlab-org/growth/-/epics/14)
* [x] Look into [Expansion team year recap](https://gitlab.com/gitlab-org/growth/product/issues/303)
* [x] Look into [Tim’s Expansion Epic](https://gitlab.com/groups/gitlab-org/-/epics/2358)
* [x] Go through the recordings of user testing interviews + make notes (gitlab-org/ux-research#574)
* [ ] Look into Nick's [Solution discovery Mural](https://app.mural.co/t/gitlab2474/m/gitlab2474/1575635788584/270d8ed1cc71cef4977b96ce801c4d7c57c34cff)
* [ ] Read the [Forrester report on GitLab](https://about.gitlab.com/analysts/forrester-cloudci19/)
* [ ] Explore https://pageflows.com/
* [ ] Read 3 growth blog posts
* [ ] [Documenting changes issue](https://gitlab.com/gitlab-org/gitlab-design/issues/775#note_260811515)



---
Unplanned tasks:

* [x] Look into gitlab-org/growth/product#785
* [x] Look into https://gitlab.com/gitlab-org/growth/product/issues/800


Ongoing tasks:

* Think about the [Growth UX page content](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/growth/)
* Dogfood design management (gitlab-com/www-gitlab-com#5218)
* Research [code-based design tools](https://gitlab.com/gitlab-org/gitlab-design/issues/241)
* [Ideas for re-engagement of zombie customers](https://gitlab.com/gitlab-org/growth/product/issues/46)
* Map GitLab's business model (gitlab-org/gitlab-design#803)


Comments:

* *Back from holiday, lots of catching up to do.*
* *Need to take this into account when planning ahead in the future.*