WEEK 49 (2019), Milestone [12.6](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&assignee_username=matejlatin&milestone_title=12.6) - W3

* [x] Update the renewal banner - wrap up (gitlab-org/growth/product#102)
  * [x] Update the designs
  * [x] Add designs to the issue, update the desc. with the solution
* [x] Update the auto-renewal banner - wrap up (gitlab-org/growth/product#143)
  * [x] Update the description with the solution
  * [x] Add new illustrations to the SVG library
  * [x] Resolve the "instructions" discussion - moved to a new issue gitlab-org/growth/product#277
* [x] MR with no pipelines, suggest a pipeline - wrap up (gitlab-org/growth/product#176)
* [x] Guidance on true-up screen - wrap up (gitlab-org/growth/engineering#40)
* [x] Read 3 growth blog posts
* [x] Feedback and possible updates for the issue board onboarding tests (https://gitlab.com/gitlab-org/growth/ui-ux/issues/21)
* [x] Check if we have default/recommended artboard widths documented anywhere (gitlab-org/gitlab-design#756)
* [x] Add specs for date range picker (gitlab-org/gitlab-design#714)
  * [x] Add changes to a new Sketch file
  * [x] Create specs and upload once reviewed
  * [x] Add the demo to the Pajamas page (gitlab-org/gitlab-services/design.gitlab.com!1634)
* [ ] UX scorecard, pt. 1: Upgrade my account (gitlab-org/growth/product#113)
  * [x] Set up a local instance of GitLab + add a mid-tier license
  * [ ] Map the journey to upgrade the account
  * [ ] Grade the experience
  * [ ] Video-document the experience

---
Unplanned tasks:

* [x] Look into gitlab-org/gitlab#30665
* [x] Look into gitlab-org/customers-gitlab-com#812
* [x] Check the [recommendations for trial improvements](https://gitlab.com/groups/gitlab-org/growth/-/epics/7#note_253361346)
* [x] Look into gitlab-org/growth/product#93
* [x] Look into [MR without pipelines epic](https://gitlab.com/groups/gitlab-org/growth/-/epics/14)

Ongoing tasks:

* UX [Buddy to Sunjung](https://gitlab.com/gitlab-org/gitlab-design/issues/713), until Dec 4
* Dogfood design management (gitlab-com/www-gitlab-com#5218)
* Research [code-based design tools](https://gitlab.com/gitlab-org/gitlab-design/issues/241)
* [Ideas for re-engagement of zombie customers](https://gitlab.com/gitlab-org/growth/product/issues/46)


Comments:

* *Did a lot of work this week, was able to move a couple of issues forward.*
* *As a consequence, I wasn't able to do all that I planned for the UX scorecard. Moving to next week.*